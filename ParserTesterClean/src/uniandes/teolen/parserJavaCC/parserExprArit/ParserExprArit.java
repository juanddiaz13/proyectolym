/* Generated By:JavaCC: Do not edit this line. ParserExprArit.java */
package uniandes.teolen.parserJavaCC.parserExprArit;

public class ParserExprArit implements ParserExprAritConstants {
  public static int contV = 0;
  public static int contF = 0;
  public static String[] vars = new String[20];
  public static String[] functs = new String [20];
  public static void main(String args []) throws ParseException
  {
    ParserExprArit parser = new ParserExprArit(System.in);
    while (true)
    {
      System.out.println("Reading from standard input...");
      System.out.print("Enter an expression like \u005c"1+(2+3)*4;\u005c" :");
      try
      {
        switch (ParserExprArit.one_line())
        {
          case 0 :
          System.out.println("OK.");
          break;
          case 1 :
          System.out.println("Goodbye.");
          break;
          default :
          break;
        }
      }
      catch (Exception e)
      {
        System.out.println("NOK.");
        System.out.println(e.getMessage());
        ParserExprArit.ReInit(System.in);
      }
      catch (Error e)
      {
        System.out.println("Oops.");
        System.out.println(e.getMessage());
        break;
      }
    }
  }

  static final public int one_line() throws ParseException, Exception {
    label_1:
    while (true) {
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case VAR:
        ;
        break;
      default:
        jj_la1[0] = jj_gen;
        break label_1;
      }
      sum();
    }
    label_2:
    while (true) {
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case DEFINE:
        ;
        break;
      default:
        jj_la1[1] = jj_gen;
        break label_2;
      }
      funct();
    }
    print();
  {if (true) return 0;}
    throw new Error("Missing return statement in function");
  }

  static final public void sum() throws ParseException {
        Token s=null;
        String nombreVariable;
    jj_consume_token(VAR);
    s = jj_consume_token(NOM);
    jj_consume_token(INITIALIZE);
    jj_consume_token(CONSTANT);
                nombreVariable = s.image;
                boolean existe = buscarVariable(nombreVariable);
                if(existe)
                {if (true) throw new ParseException("Ya existe una variable con el nombre: " + nombreVariable);}
                else
                {
                        vars[contV] = nombreVariable + "";
                        contV++;
                }
  }

  static final public void funct() throws ParseException, Exception {
        Token s=null,p=null;
        String nombreFuncion;
        String params = "";
        int parame = 0;
    jj_consume_token(DEFINE);
    s = jj_consume_token(NOM);
    jj_consume_token(PARENTEIZQ);
    label_3:
    while (true) {
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case NOM:
        ;
        break;
      default:
        jj_la1[2] = jj_gen;
        break label_3;
      }
      params = agParam(params);
      parame = parametrosA(parame);
      label_4:
      while (true) {
        switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
        case COMMA:
          ;
          break;
        default:
          jj_la1[3] = jj_gen;
          break label_4;
        }
        jj_consume_token(COMMA);
        params = agParam(params);
        parame = parametrosA(parame);
      }
    }
    jj_consume_token(PARENTEDER);
    jj_consume_token(DOSPUNTOS);
    nombreFuncion = s.image;
    boolean buscar = buscarFunct(nombreFuncion);
    if(buscar)
                {if (true) throw new ParseException("Ya existe una funci\u00c3\u00b3n con el nombre: " + nombreFuncion);}
        else
                {
                        functs[contF] = nombreFuncion + ";" + parame + ";" + params;
                        System.out.println(functs[contF]+ "THIS IS IT ");
                        contF++;
                        cuerpo();
                }
  }

  static final public String agParam(String co) throws ParseException {
  Token s=null;
  String re = co;
    s = jj_consume_token(NOM);
          re = co + s.image + "_";
                {if (true) return re;}
    throw new Error("Missing return statement in function");
  }

  static final public void cuerpo() throws ParseException, Exception {
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case BRACKIZQ:
      jj_consume_token(BRACKIZQ);
      break;
    default:
      jj_la1[4] = jj_gen;
      ;
    }
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case IFL:
      jj_consume_token(IFL);
      iff();
      break;
    case CONSTANT:
      jj_consume_token(CONSTANT);
      break;
    case NOM:
      buscarVariableFuncionParam();
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case PARENTEIZQ:
        jj_consume_token(PARENTEIZQ);
        label_5:
        while (true) {
          switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
          case CONSTANT:
          case NOM:
            ;
            break;
          default:
            jj_la1[5] = jj_gen;
            break label_5;
          }
          switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
          case NOM:
            buscarVariableFuncionParam();
            break;
          case CONSTANT:
            jj_consume_token(CONSTANT);
            break;
          default:
            jj_la1[6] = jj_gen;
            jj_consume_token(-1);
            throw new ParseException();
          }
          label_6:
          while (true) {
            switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
            case COMMA:
              ;
              break;
            default:
              jj_la1[7] = jj_gen;
              break label_6;
            }
            jj_consume_token(COMMA);
            switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
            case NOM:
              buscarVariableFuncionParam();
              break;
            case CONSTANT:
              jj_consume_token(CONSTANT);
              break;
            default:
              jj_la1[8] = jj_gen;
              jj_consume_token(-1);
              throw new ParseException();
            }
          }
        }
        jj_consume_token(PARENTEDER);
        break;
      default:
        jj_la1[9] = jj_gen;
        ;
      }
      break;
    default:
      jj_la1[10] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case BRACKDER:
      jj_consume_token(BRACKDER);
      break;
    default:
      jj_la1[11] = jj_gen;
      ;
    }
    label_7:
    while (true) {
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case OPARIT:
        ;
        break;
      default:
        jj_la1[12] = jj_gen;
        break label_7;
      }
      jj_consume_token(OPARIT);
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case BRACKIZQ:
        jj_consume_token(BRACKIZQ);
        break;
      default:
        jj_la1[13] = jj_gen;
        ;
      }
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case CONSTANT:
        jj_consume_token(CONSTANT);
        break;
      case NOM:
        buscarVariableFuncionParam();
        switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
        case PARENTEIZQ:
          jj_consume_token(PARENTEIZQ);
          label_8:
          while (true) {
            switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
            case CONSTANT:
            case NOM:
              ;
              break;
            default:
              jj_la1[14] = jj_gen;
              break label_8;
            }
            switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
            case NOM:
              buscarVariableFuncionParam();
              break;
            case CONSTANT:
              jj_consume_token(CONSTANT);
              break;
            default:
              jj_la1[15] = jj_gen;
              jj_consume_token(-1);
              throw new ParseException();
            }
            label_9:
            while (true) {
              switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
              case COMMA:
                ;
                break;
              default:
                jj_la1[16] = jj_gen;
                break label_9;
              }
              jj_consume_token(COMMA);
              switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
              case NOM:
                buscarVariableFuncionParam();
                break;
              case CONSTANT:
                jj_consume_token(CONSTANT);
                break;
              default:
                jj_la1[17] = jj_gen;
                jj_consume_token(-1);
                throw new ParseException();
              }
            }
          }
          jj_consume_token(PARENTEDER);
          break;
        default:
          jj_la1[18] = jj_gen;
          ;
        }
        break;
      case IFL:
        jj_consume_token(IFL);
        iff();
        break;
      default:
        jj_la1[19] = jj_gen;
        jj_consume_token(-1);
        throw new ParseException();
      }
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case BRACKDER:
        jj_consume_token(BRACKDER);
        break;
      default:
        jj_la1[20] = jj_gen;
        ;
      }
    }

  }

  static final public void iff() throws ParseException, Exception {
    jj_consume_token(PARENTEIZQ);
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case NOT:
      jj_consume_token(NOT);
      break;
    default:
      jj_la1[21] = jj_gen;
      ;
    }
    cuerpo();
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case OPLOGIC:
      jj_consume_token(OPLOGIC);
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case NOT:
        jj_consume_token(NOT);
        break;
      default:
        jj_la1[22] = jj_gen;
        ;
      }
      cuerpo();
      break;
    default:
      jj_la1[23] = jj_gen;
      ;
    }
    jj_consume_token(COMMA);
    cuerpo();
    jj_consume_token(COMMA);
    cuerpo();
    jj_consume_token(PARENTEDER);

  }

  static final public boolean buscarVariableFuncionParam() throws ParseException, Exception {
  Token s=null;
  boolean res = false;
  String pCosa = "";
    s = jj_consume_token(NOM);
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case OPARIT:
      jj_consume_token(OPARIT);
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case CONSTANT:
        jj_consume_token(CONSTANT);
        break;
      case NOM:
        buscarVariableFuncionParam();
        break;
      default:
        jj_la1[24] = jj_gen;
        jj_consume_token(-1);
        throw new ParseException();
      }
      break;
    default:
      jj_la1[25] = jj_gen;
      ;
    }
    pCosa = s.image + "";
    res = buscarVariable(pCosa);
    if(res) { {if (true) return true;} }
    res = buscarFunct(pCosa);
    if(res) { {if (true) return true;} }
    res = buscarParam(pCosa);
    if(res) { {if (true) return true;} }

    if(!res) { {if (true) throw new Exception ("Para la palabra: " + pCosa + " no se ha encontrado una variable, funci\u00c3\u00b3n o par\u00c3\u00a1metro");}}

    {if (true) return res;}
    throw new Error("Missing return statement in function");
  }

  static final public boolean buscarParam(String pCosa) throws ParseException {
  boolean encontro = false;
          for(int e=0; e<functs.length && functs[e]!=null;e++)
         {
                String este = functs[e].split(";")[1];
                int num = Integer.parseInt(este);
                if(num==1)
                {
                        String rest = functs[e].split(";")[2].substring(0,functs[e].split(";")[2].length()-1);
                        System.out.println("esto es xpopx: " + rest);
                        if(rest.equals(pCosa))
                        {if (true) return true;}
                }
                if(num >1)
                {
                  System.out.println("lega1llla");
                  String este1[] = functs[e].split(";")[2].substring(0,functs[e].split(";")[2].length()-1).split(".");
          String rest2[] = (functs[e].split(";")[2].substring(0,functs[e].split(";")[2].length()-1)).split("_");
                 for(int j =0;(rest2)[j]!=null ;j++)
                 {
                   if((rest2)[j].equals(pCosa))
                   {if (true) return true;}
             }
        }
     }
     {if (true) return encontro;}
    throw new Error("Missing return statement in function");
  }

  static final public int parametrosA(int num) throws ParseException {
  int lol = num + 1;
                {if (true) return lol;}
    throw new Error("Missing return statement in function");
  }

  static final public boolean buscarFunct(String pNombre) throws ParseException {
  boolean encontro= false;
        for(int x=0;x<functs.length && functs[x]!=null;x++)
        {
          String aux[] = functs[x].split(";");
          if(aux[0]!=null && aux[0].equals(pNombre))
          {
            {if (true) return true;}
          }
    }
    {if (true) return encontro;}
    throw new Error("Missing return statement in function");
  }

  static final public boolean buscarVariable(String pNombre) throws ParseException {
        boolean encontro = false;
        for(int x=0;x<vars.length && vars[x]!=null;x++)
        {
                if(vars[x].equals(pNombre))
                {
                        {if (true) return true;}
                }
        }
        {if (true) return encontro;}
    throw new Error("Missing return statement in function");
  }

  static final public void print() throws ParseException, Exception {
    jj_consume_token(PRINT);
    jj_consume_token(PARENTEIZQ);
    cuerpo();
    jj_consume_token(PARENTEDER);

  }

  static private boolean jj_initialized_once = false;
  /** Generated Token Manager. */
  static public ParserExprAritTokenManager token_source;
  static SimpleCharStream jj_input_stream;
  /** Current token. */
  static public Token token;
  /** Next token. */
  static public Token jj_nt;
  static private int jj_ntk;
  static private int jj_gen;
  static final private int[] jj_la1 = new int[26];
  static private int[] jj_la1_0;
  static {
      jj_la1_init_0();
   }
   private static void jj_la1_init_0() {
      jj_la1_0 = new int[] {0x200,0x400,0x800000,0x8000,0x10000,0x880000,0x880000,0x8000,0x880000,0x1000,0x8c0000,0x20000,0x40,0x10000,0x880000,0x880000,0x8000,0x880000,0x1000,0x8c0000,0x20000,0x100,0x100,0x80,0x880000,0x40,};
   }

  /** Constructor with InputStream. */
  public ParserExprArit(java.io.InputStream stream) {
     this(stream, null);
  }
  /** Constructor with InputStream and supplied encoding */
  public ParserExprArit(java.io.InputStream stream, String encoding) {
    if (jj_initialized_once) {
      System.out.println("ERROR: Second call to constructor of static parser.  ");
      System.out.println("       You must either use ReInit() or set the JavaCC option STATIC to false");
      System.out.println("       during parser generation.");
      throw new Error();
    }
    jj_initialized_once = true;
    try { jj_input_stream = new SimpleCharStream(stream, encoding, 1, 1); } catch(java.io.UnsupportedEncodingException e) { throw new RuntimeException(e); }
    token_source = new ParserExprAritTokenManager(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 26; i++) jj_la1[i] = -1;
  }

  /** Reinitialise. */
  static public void ReInit(java.io.InputStream stream) {
     ReInit(stream, null);
  }
  /** Reinitialise. */
  static public void ReInit(java.io.InputStream stream, String encoding) {
    try { jj_input_stream.ReInit(stream, encoding, 1, 1); } catch(java.io.UnsupportedEncodingException e) { throw new RuntimeException(e); }
    token_source.ReInit(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 26; i++) jj_la1[i] = -1;
  }

  /** Constructor. */
  public ParserExprArit(java.io.Reader stream) {
    if (jj_initialized_once) {
      System.out.println("ERROR: Second call to constructor of static parser. ");
      System.out.println("       You must either use ReInit() or set the JavaCC option STATIC to false");
      System.out.println("       during parser generation.");
      throw new Error();
    }
    jj_initialized_once = true;
    jj_input_stream = new SimpleCharStream(stream, 1, 1);
    token_source = new ParserExprAritTokenManager(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 26; i++) jj_la1[i] = -1;
  }

  /** Reinitialise. */
  static public void ReInit(java.io.Reader stream) {
    jj_input_stream.ReInit(stream, 1, 1);
    token_source.ReInit(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 26; i++) jj_la1[i] = -1;
  }

  /** Constructor with generated Token Manager. */
  public ParserExprArit(ParserExprAritTokenManager tm) {
    if (jj_initialized_once) {
      System.out.println("ERROR: Second call to constructor of static parser. ");
      System.out.println("       You must either use ReInit() or set the JavaCC option STATIC to false");
      System.out.println("       during parser generation.");
      throw new Error();
    }
    jj_initialized_once = true;
    token_source = tm;
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 26; i++) jj_la1[i] = -1;
  }

  /** Reinitialise. */
  public void ReInit(ParserExprAritTokenManager tm) {
    token_source = tm;
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 26; i++) jj_la1[i] = -1;
  }

  static private Token jj_consume_token(int kind) throws ParseException {
    Token oldToken;
    if ((oldToken = token).next != null) token = token.next;
    else token = token.next = token_source.getNextToken();
    jj_ntk = -1;
    if (token.kind == kind) {
      jj_gen++;
      return token;
    }
    token = oldToken;
    jj_kind = kind;
    throw generateParseException();
  }


/** Get the next Token. */
  static final public Token getNextToken() {
    if (token.next != null) token = token.next;
    else token = token.next = token_source.getNextToken();
    jj_ntk = -1;
    jj_gen++;
    return token;
  }

/** Get the specific Token. */
  static final public Token getToken(int index) {
    Token t = token;
    for (int i = 0; i < index; i++) {
      if (t.next != null) t = t.next;
      else t = t.next = token_source.getNextToken();
    }
    return t;
  }

  static private int jj_ntk() {
    if ((jj_nt=token.next) == null)
      return (jj_ntk = (token.next=token_source.getNextToken()).kind);
    else
      return (jj_ntk = jj_nt.kind);
  }

  static private java.util.List<int[]> jj_expentries = new java.util.ArrayList<int[]>();
  static private int[] jj_expentry;
  static private int jj_kind = -1;

  /** Generate ParseException. */
  static public ParseException generateParseException() {
    jj_expentries.clear();
    boolean[] la1tokens = new boolean[24];
    if (jj_kind >= 0) {
      la1tokens[jj_kind] = true;
      jj_kind = -1;
    }
    for (int i = 0; i < 26; i++) {
      if (jj_la1[i] == jj_gen) {
        for (int j = 0; j < 32; j++) {
          if ((jj_la1_0[i] & (1<<j)) != 0) {
            la1tokens[j] = true;
          }
        }
      }
    }
    for (int i = 0; i < 24; i++) {
      if (la1tokens[i]) {
        jj_expentry = new int[1];
        jj_expentry[0] = i;
        jj_expentries.add(jj_expentry);
      }
    }
    int[][] exptokseq = new int[jj_expentries.size()][];
    for (int i = 0; i < jj_expentries.size(); i++) {
      exptokseq[i] = jj_expentries.get(i);
    }
    return new ParseException(token, exptokseq, tokenImage);
  }

  /** Enable tracing. */
  static final public void enable_tracing() {
  }

  /** Disable tracing. */
  static final public void disable_tracing() {
  }

}
